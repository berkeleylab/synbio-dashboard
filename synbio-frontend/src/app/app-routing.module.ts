import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DataTableViewComponent} from "./synbio/view/datatableview.component";
import {CalibanGuard} from "./guard/caliban.guard";

const routes: Routes = [
  { path:'', component: DataTableViewComponent, canActivate: [CalibanGuard]},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
