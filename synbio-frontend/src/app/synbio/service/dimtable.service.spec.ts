import {TestBed} from '@angular/core/testing';

import {DimtableService} from './dimtable.service';

describe('DimtableService', () => {
  let service: DimtableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DimtableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
