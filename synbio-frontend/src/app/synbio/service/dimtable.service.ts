import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {KeycloakProfile} from "keycloak-js";

export interface BatchDim {
  pmoProposalId: number,
  name: string;
  status: string;
  batchId: number;
  counts: number;
  basePairs: number;
  deliverableCounts: number;
  deliverableBasePairs: number;
  pCompleted: number;
  pInProgress: number;
  pWaiting: number;
  pAbandoned: number;
  constructDims: Array<ConstructDim>;
}

export interface ConstructDim {
  designId: number;
  uName: string;
  fdStatus: string;
  fdCreated: string;
  jgiName: string;
  orderedDate: string;
  receivedDate: string;
  completedDate: string;
  fdSize: number;
}


@Injectable({
  providedIn: 'root'
})
export class DimtableService {
  static DASHBOARD_V1_API: string = "synbio-view/api/v1";
  static LOGOUT_URL: string = "https://auth.jgi.lbl.gov/auth/realms/jgi-web/protocol/openid-connect/logout";

  //static LOGOUT_URL: string = "http://localhost:8180/auth/realms/jgi-web/protocol/openid-connect/logout";
  constructor(private http: HttpClient) {
  }

  public getDWBatchData(): Observable<BatchDim[]> {
    let url = `${(DimtableService.DASHBOARD_V1_API)}/batches`;
    return this.http.get<BatchDim[]>(url);
  }

  public loadUserProfile(): Observable<KeycloakProfile> {
    let url = `${(DimtableService.DASHBOARD_V1_API)}/account`;
    return this.http.get<KeycloakProfile>(url);
  }

  getBatchesForContactEmail(email: string): Observable<BatchDim[]> {
    let url = `${(DimtableService.DASHBOARD_V1_API)}/batches-for-contact-email?email=${email}`;
    return this.http.get<BatchDim[]>(url);
  }


}
