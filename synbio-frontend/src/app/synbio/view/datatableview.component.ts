import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {BatchDim, DimtableService} from '../service/dimtable.service';
// import {KeycloakService} from "keycloak-angular";
import {KeycloakProfile} from "keycloak-js";
import {FormBuilder} from '@angular/forms';
import {AuthService} from "../../guard/auth.service";


@Component({
  templateUrl: './datatableview.component.html',
  styleUrls: ['./datatableview.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DataTableViewComponent implements OnInit {
  // private cookieValue: string;
  public isAdminUser = false;
  public userProfile: KeycloakProfile | null = null;

  batches: BatchDim[] = [];
  selectedBatch: BatchDim | undefined;
  batch_cols: any = [];
  constr_cols: any[] = [];
  formGroup;

  constructor(
    // private keycloakService: KeycloakService,
    private tableService: DimtableService,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.formGroup = this.formBuilder.group({
      email: '',
    });
  }

  onSubmit(formData: { [x: string]: any; }) {
    this.selectedBatch = undefined;
    var email = formData['email'];
    this.tableService.getBatchesForContactEmail(email).subscribe(res => {
      this.batches = res;
    })
  }

  logout() {
    //this.keycloakService.logout(DimtableService.LOGOUT_URL);
    // this.keycloakService.logout()
    this.authService.logout()
  }

  // login() {
  //   this.authService.login()
  // }


  // private init() {
  //   this.tableService.getDWBatchData().subscribe(res => {
  //     this.batches = res;
  //   }
  //   // ,
  //   //   error => {
  //   //     this.handleError(error.error)
  //   //   }
  //     );
  //
  //   // this.tableService.getDWConstructData().subscribe(res => {
  //   //   this.constructs = res;
  //   // });
  // }
  // // private handleError(error: any) {
  // //   this.displayError(error.code + ' ' + error.reason + ". " + error.message)
  // // }
  // //
  // // private displayError(message: string) {
  // //   this.snackBar.open(message, 'Close', { duration: 5000})
  // // }

  public async ngOnInit() {
    //window.location.href = 'https://signon.jgi.doe.gov/signon';
    // this.isLoggedIn = await this.keycloakService.isLoggedIn();
    // this.isAdminUser = await this.keycloakService.isUserInRole('synbio-admin')

      //this.userProfile = await this.keycloakService.loadUserProfile();
      this.tableService.loadUserProfile().subscribe(res => {
        this.userProfile = res;
        //this.selectedBatch = this.batches[0];
      })

    this.tableService.getDWBatchData().subscribe(res => {
      this.batches = res;
      //this.selectedBatch = this.batches[0];
    })

    this.batch_cols = [
      {field: 'pmoProposalId', header: 'PMO Proposal ID'},
      {field: 'name', header: 'Batch Name'},
      // { field: 'batchId', header: 'SynTrack Design Id' },
      {field: 'status', header: 'Status'},
      {field: 'counts', header: 'Counts', tooltip: 'Total # of constructs requested in batch'},
      {field: 'basePairs', header: 'Base Pairs', tooltip: 'Total amount of DNA requested in batch'},
      {field: 'deliverableCounts', header: 'Deliverable Counts', tooltip: 'Total # completed constructs in batch'},
      {field: 'deliverableBasePairs', header: 'Deliverable Base Pairs', tooltip: 'Total amount of DNA delivered'},
      {field: 'pCompleted', header: '% Completed', tooltip: '% of successful constructs of the total requested'},
      {field: 'pInProgress', header: '% In Progress', tooltip: '% of total constructs still actively being worked on'},
      {field: 'pWaiting', header: '% Waiting', tooltip: '% of constructs ordered but not yet received from vendor'},
      {field: 'pAbandoned', header: '% Abandoned', tooltip: '% of constructs that failed in the process and will not get delivered'}
    ];

    this.constr_cols = [
      // { field: 'designId', header: 'SynTrack Design Id' },
      {field: 'uName', header: 'Construct Name'},
      {field: 'fdStatus', header: 'Status'},
      // { field: 'fdCreated', header: 'Created Date'},
      {field: 'jgiName', header: 'JGI Name'},
      // { field: 'orderedDate', header: 'Order Date' },
      {field: 'receivedDate', header: 'Received Date', tooltip: 'Date fragment received from vendor'},
      {field: 'completedDate', header: 'Completed Date', tooltip: 'Date sequence verification confirmed'}
      // {field: 'fdSize', header: 'Base Pairs', tooltip: 'Size of construct'}
    ];
  }


}


