import {KeycloakService} from "keycloak-angular";

export function initializeKeycloak (
  keycloak: KeycloakService
) {
  return () =>
    keycloak.init({
      config: {
        // url: 'http://keycloak:8080/auth/',
        url: 'https://auth.jgi.doe.gov/auth/',
        realm: 'jgi-web',
        clientId: 'jgi-synbio-frontend',
      }
      ,
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html'
      }
    });
}
