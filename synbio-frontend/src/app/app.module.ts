import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {TableModule} from 'primeng/table';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DimtableService} from './synbio/service/dimtable.service';
import {DataTableViewComponent} from "./synbio/view/datatableview.component";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CookieService} from "ngx-cookie-service";
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TooltipModule } from "primeng/tooltip";

export function kcFactory(keycloakService: KeycloakService) {
  return () => keycloakService.init();
}

@NgModule({
  declarations: [
    AppComponent,
    DataTableViewComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    KeycloakAngularModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule
  ],
  providers: [DimtableService,KeycloakService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
