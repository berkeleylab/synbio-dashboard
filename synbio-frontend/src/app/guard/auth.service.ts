import {Inject, Injectable} from '@angular/core';
import {CookieService} from "ngx-cookie-service";
import {DOCUMENT} from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authenticated = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string | null = null;

  constructor(private cookieService: CookieService,
              @Inject(DOCUMENT) private document: Document) {
  }

  isLoggedIn(): boolean {
    this.authenticated = this.cookieService.check('jgi_session')
    return this.authenticated;
  }

  login(): boolean {
    //javascript: if(top.window.location.href.search('accessDenied')==-1)
    // { document.cookie='jgi_return=' + escape(top.window.location.href) + '; domain=.jgi.doe.gov; path=/;';  }
    // top.window.location.href='https://signon.jgi.doe.gov/signon';
    this.cookieService.set('jgi_return', <string>this.redirectUrl, undefined, '/', '.jgi.doe.gov');
    this.document.location.href = 'https://signon.jgi.doe.gov/signon';
    return true;
  }

  logout(): void {
    //javascript: if(top.window.location.href.search('accessDenied')==-1) {
    // document.cookie='jgi_return=' + escape(top.window.location.href) + '; domain=.jgi.doe.gov; path=/;';
    // document.cookie='session=; domain=.jgi.doe.gov; path=/;';  } top.window.location.href='https://signon.jgi.doe.gov/signon/destroy';
    this.cookieService.set('jgi_return', document.location.origin, undefined, '/', '.jgi.doe.gov');
    this.cookieService.set('session', '', undefined, '/', '.jgi.doe.gov');
    this.document.location.href = 'https://signon.jgi.doe.gov/signon/destroy';
  }
}
