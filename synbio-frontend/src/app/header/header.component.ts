import {Component, OnInit} from '@angular/core';
import {AuthService} from "../guard/auth.service";
import {DimtableService} from "../synbio/service/dimtable.service";
import {KeycloakProfile} from "keycloak-js";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public userProfile: KeycloakProfile | null = null;

  constructor(
    private authService: AuthService,
    private tableService: DimtableService
  ) {
  }

  logout() {
    //this.keycloakService.logout(DimtableService.LOGOUT_URL);
    // this.keycloakService.logout()
    this.authService.logout()
  }

  public async ngOnInit() {
    //this.userProfile = await this.keycloakService.loadUserProfile();
    this.tableService.loadUserProfile().subscribe(res => {
      this.userProfile = res;
      //this.selectedBatch = this.batches[0];
    })
  }

}
