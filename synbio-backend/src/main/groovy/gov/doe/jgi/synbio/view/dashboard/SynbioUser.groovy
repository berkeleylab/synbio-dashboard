package gov.doe.jgi.synbio.view.dashboard


import groovy.transform.Canonical

@Canonical
class SynbioUser {
    Integer user_id
    String username
    String email

    @Override
    public String toString() {
        return "SynbioUser{" +
                "user_id=" + user_id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
