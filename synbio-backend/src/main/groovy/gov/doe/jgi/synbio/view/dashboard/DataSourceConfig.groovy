package gov.doe.jgi.synbio.view.dashboard


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.env.Environment
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

import javax.sql.DataSource

@Configuration
class DataSourceConfig {
    @Autowired
    Environment env

    @Bean(name = "synbioDataSource")
    @Qualifier("synbioDataSource")
    @Primary
    DataSource synbioDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.url(env.getProperty("spring.datasource.url"))
        dataSourceBuilder.username(env.getProperty("spring.datasource.username"))
        dataSourceBuilder.password(env.getProperty("spring.datasource.password"))
        dataSourceBuilder.driverClassName(env.getProperty("spring.datasource.driverClassName"))
        DataSource source = dataSourceBuilder.build()
        return source
    }

    @Bean(name = "synbioJdbcTemplate")
    NamedParameterJdbcTemplate ynbioJdbcTemplate(
            @Qualifier("synbioDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }

    @Bean(name = "dwDataSource")
    @Qualifier("dwDataSource")
    DataSource dwDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.url(env.getProperty("dwDataSource.url"))
        dataSourceBuilder.username(env.getProperty("dwDataSource.username"))
        dataSourceBuilder.password(env.getProperty("dwDataSource.password"))
        dataSourceBuilder.driverClassName(env.getProperty("dwDataSource.driverClassName"))
        DataSource source = dataSourceBuilder.build()
        return source
    }

    @Bean(name = "dwJdbcTemplate")
    NamedParameterJdbcTemplate dwJdbcTemplate(
            @Qualifier("dwDataSource") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }


}
