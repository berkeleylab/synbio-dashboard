package gov.doe.jgi.synbio.view.dashboard

import groovy.transform.Canonical

@Canonical
class ChartFeeder {
    String name
    List<List<Object>> chart_data
}
