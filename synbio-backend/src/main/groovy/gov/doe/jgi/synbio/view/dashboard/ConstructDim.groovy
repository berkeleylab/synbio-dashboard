package gov.doe.jgi.synbio.view.dashboard


import groovy.transform.Canonical

@Canonical
class ConstructDim {
    Integer designId
    String fdStatus
    String fdCreated
    String uName
    String jgiName
    String orderedDate
    String receivedDate
    String completedDate
    Long fdSize


    @Override
    public String toString() {
        return "ConstructDim{" +
                "designId=" + designId +
                ", fdStatus='" + fdStatus + '\'' +
                ", fdCreated='" + fdCreated + '\'' +
                ", jgiName='" + jgiName + '\'' +
                ", uName='" + uName + '\'' +
                ", orderedDate='" + orderedDate + '\'' +
                ", receivedDate='" + receivedDate + '\'' +
                ", completedDate='" + completedDate + '\'' +
                ", fdSize=" + fdSize +
                '}';
    }
}
