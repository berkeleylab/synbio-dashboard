package gov.doe.jgi.synbio.view.dashboard


import gov.doe.jgi.synbio.view.utils.CalibanInvalidSessionException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

import java.sql.ResultSet
import java.sql.SQLException
import java.time.Duration

@Service
class DashboardDataServiceImpl implements DashboardDataService {
    static final Logger log = LoggerFactory.getLogger(DashboardDataServiceImpl.class)


//    private static ObjectMapper objectMapper
//    static {
//        Jackson2ObjectMapperBuilder jacksonBuilder = new Jackson2ObjectMapperBuilder()
//        //jacksonBuilder.propertyNamingStrategy = new LowerCaseWithHyphensStrategy()
//        objectMapper = jacksonBuilder.build()
//    }

    @Autowired
    WebClient wsAccessClient

    @Autowired
    NamedParameterJdbcTemplate synbioJdbcTemplate

    @Autowired
    NamedParameterJdbcTemplate dwJdbcTemplate

    @Override
    Collection<BatchDim> getBatches() {
        getBatchesForContactEmail('yung6@llnl.gov')
    }

    @Override
    Collection<BatchDim> getBatchesForContactEmail(String email) {
        assert email
        log.info("email: $email")
        Collection<Proposals> proposals = getProposalsForContactEmail(email)
        if (!proposals)
            return null
        List<Long> pmoPropIds = proposals*.proposalId
        Collection<BatchDim> batches = getBatchesForPmoPropIds(pmoPropIds)
        Map<Integer, Collection<ConstructDim>> constructs = getConstructsForPmoPropIds(pmoPropIds)
        batches.each { batchDim ->
            Collection<ConstructDim> constructDims = constructs[batchDim.batchId]
            assert constructDims
            batchDim.update(constructDims)
        }
    }

    @Override
    Collection<Proposals> getProposalsForContactEmail(String email) {
        assert email
        log.info("email: $email")
        MapSqlParameterSource parameters = new MapSqlParameterSource()
        parameters.addValue("email", email)
        String sql = DesignsQueryBuilder.getProposalsForContactEmail()
        List<Proposals> result = dwJdbcTemplate.query(sql, parameters,
                new RowMapper<Proposals>() {
                    @Override
                    Proposals mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new Proposals(
                                proposalId: rs.getLong('PROPOSAL_ID'),
                                focusArea: rs.getString('FOCUS_AREA'),
                                projectType: rs.getString('PROJECT_TYPE'),
                                title: rs.getString('TITLE'))
                    }
                })
        log.debug "$result"
        log.info "retrieved ${result?.size()} proposal(s) for email: $email"
        result
    }

    @Override
    KeycloakProfile getKeycloakProfile(String ssoSessionToken) {
        assert ssoSessionToken
        ///api/sessions/597157da7d3b3ed47e7a3caceeec15ac
        if (!ssoSessionToken.startsWith('/api/sessions'))
            throw new CalibanInvalidSessionException(ssoSessionToken)

        String path = "${ssoSessionToken}.json"

        WebClient.RequestHeadersUriSpec spec = wsAccessClient.get()
                .uri({ uriBuilder ->
                    uriBuilder.path(path)
                            .build()
                })
        JgiSession content = spec.accept(MediaType.APPLICATION_JSON).retrieve().
                bodyToMono(new ParameterizedTypeReference<JgiSession>() {}).timeout(Duration.ofSeconds(60)).block()
        SsoUser ssoUser = content?.ssoUser
        if (!ssoUser)
            throw new CalibanInvalidSessionException(ssoSessionToken)
        KeycloakProfile keycloakProfile = ssoUser.toKeycloakProfile()
        keycloakProfile.with {
            SynbioUser synbioUser = getActiveStaffSynbioUsersForContactEmail(email).find { true }
            enabled = synbioUser != null
        }
        keycloakProfile
    }

    @Override
    Collection<SynbioUser> getActiveStaffSynbioUsersForContactEmail(String email) {
        assert email
        log.debug("email: $email")
        MapSqlParameterSource parameters = new MapSqlParameterSource()
        parameters.addValue("email", email)
        String sql = DesignsQueryBuilder.getActiveStaffSynbioUsersForContactEmail()
        List<SynbioUser> result = synbioJdbcTemplate.query(sql, parameters,
                new RowMapper<SynbioUser>() {
                    @Override
                    SynbioUser mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new SynbioUser(
                                user_id: rs.getInt('USER_ID'),
                                username: rs.getString('USERNAME'),
                                email: rs.getString('EMAIL'))
                    }
                })
        log.debug "$result"
        log.info "retrieved ${result?.size()} active staff synbio users for email: $email"
        result
    }
    static final String RECEIVED_DATE = '2020-10-01'

    private Collection<BatchDim> getBatchesForPmoPropIds(Collection<Long> pmoPropIds) {
        assert pmoPropIds
        log.info("RECEIVED_DATE: $RECEIVED_DATE")
        log.info("pmoPropIds: $pmoPropIds")
        MapSqlParameterSource parameters = new MapSqlParameterSource()
        parameters.addValue("orderedDate", RECEIVED_DATE)
        parameters.addValue("receivedDate", RECEIVED_DATE)
        parameters.addValue("pmoPropIds", pmoPropIds)
        String sql = DesignsQueryBuilder.getBatchesForPropIds()
        List<BatchDim> result = synbioJdbcTemplate.query(sql, parameters,
                new RowMapper<BatchDim>() {
                    @Override
                    BatchDim mapRow(ResultSet rs, int rowNum) throws SQLException {
                        String status = rs.getString('STATUS')
                        if (status == 'inprogress')
                            status = 'in progress'
                        return new BatchDim(
                                pmoProposalId: rs.getLong('PMO_PROPOSAL_ID'),
                                name: rs.getString('NAME'),
                                status: status,
                                batchId: rs.getInt('BATCH_ID'),
                                counts: rs.getInt('CNT_COUNT'),
                                deliverableCounts: rs.getInt('CNT_COUNT_DELIVERABLE') ?: null,
                                basePairs: rs.getInt('SUM_BPS'),
                                deliverableBasePairs: rs.getInt('SUM_BPS_DELIVERABLE') ?: null)
                    }
                })
        log.debug "$result"
        log.info "retrieved ${result?.size()} batches for proposals: $pmoPropIds"
        result
    }

    private Map<Integer, Collection<ConstructDim>> getConstructsForPmoPropIds(Collection<Long> pmoPropIds) {
        assert pmoPropIds
        log.info("RECEIVED_DATE: $RECEIVED_DATE")
        log.info("pmoPropIds: $pmoPropIds")
        MapSqlParameterSource parameters = new MapSqlParameterSource()
        parameters.addValue("orderedDate", RECEIVED_DATE)
        parameters.addValue("receivedDate", RECEIVED_DATE)
        parameters.addValue("completedDate", RECEIVED_DATE)
        parameters.addValue("pmoPropIds", pmoPropIds)
        String sql = DesignsQueryBuilder.getConstructsPropIds()
        List<ConstructDim> constructDims = synbioJdbcTemplate.query(sql, parameters,
                new RowMapper<ConstructDim>() {
                    @Override
                    ConstructDim mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new ConstructDim(
                                designId: rs.getLong('DESIGN_ID'),
                                fdStatus: rs.getString('FD_STATUS'),
                                fdCreated: rs.getString('FD_CREATED'),
                                jgiName: rs.getString('JGI_NAME'),
                                uName: rs.getString('UNAME'),
                                orderedDate: rs.getString('ORDERED_DATE'),
                                receivedDate: rs.getString('RECEIVED_DATE'),
                                completedDate: rs.getString('COMPLETED_DATE'),
                                fdSize: rs.getInt('CNST_P_SIZE'))
                    }
                })
        Map<Integer, ConstructDim> constructs = new HashMap<>().withDefault { [] }
        constructDims.each {
            constructs[it.designId] << it
        }
        log.debug "$constructs"
        log.info "retrieved ${constructs?.size()} constructs for proposals: $pmoPropIds"
        constructs
    }
}
