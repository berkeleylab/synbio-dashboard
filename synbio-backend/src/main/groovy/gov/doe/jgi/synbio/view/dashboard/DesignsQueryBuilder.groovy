package gov.doe.jgi.synbio.view.dashboard

final class DesignsQueryBuilder {
    private static final String getSql(String deliverable = '', String leftJoint = '') {
        """
        SELECT PMO_PROPOSAL_ID,
               NAME,
               STATUS,
               DESIGN_ID,
               COUNT(DELIVERABLE_ID) CNT_COUNT${deliverable},
        SUM(CNST_P_SIZE) SUM_BPS${deliverable}
        FROM
        (SELECT DS.PMO_PROPOSAL_ID,
                DS.NAME,
                DS.STATUS,
                DS.BATCH_ID,
                CS.DESIGN_ID,
                CS.DELIVERABLE_ID,
                CS.CNST_P_SIZE
        FROM DIM_CONSTRUCTS CS,
        DIM_BATCHES DS
        WHERE BATCH_ID IN
        (SELECT DISTINCT DESIGN_ID
        FROM DIM_CONSTRUCTS        
        WHERE (BATCH_LATEST_ORDERED_DATE >= :orderedDate :: DATE
                OR BATCH_LATEST_RECEIVED_DATE >= :receivedDate :: DATE) )
        AND DS.PMO_PROPOSAL_ID in (:pmoPropIds)
        AND CS.DESIGN_ID = DS.BATCH_ID
        $leftJoint
        GROUP BY CS.DESIGN_ID,
        DS.NAME,
        DS.STATUS,
        DS.BATCH_ID,
        CS.DELIVERABLE_ID,
        CS.CNST_P_SIZE
        ORDER BY DS.PMO_PROPOSAL_ID,
        DS.NAME) SUM_RECORD
        GROUP BY PMO_PROPOSAL_ID,
        NAME,
        STATUS,
        DESIGN_ID
"""
    }

    static final String getProposalsForContactEmail() {
    """
    SELECT PR.PROPOSAL_ID,
    PR.FOCUS_AREA,
    PR.PROJECT_TYPE, PR.TITLE
    FROM DW.CONTACT CONT
    JOIN DW.PROPOSAL PR ON PR.PRINCIPAL_INVESTIGATOR_ID = CONT.CONTACT_ID
    WHERE LOWER(CONT.EMAIL_ADDRESS) = LOWER(:email);
    """
    }

        static final String getActiveStaffSynbioUsersForContactEmail() {
            """
        SELECT USER_ID,
            USERNAME,
            EMAIL
        FROM DIM_USERS
        WHERE IS_ACTIVE = TRUE
            AND IS_STAFF = TRUE
            AND LOWER(EMAIL) = LOWER(:email);
        """
        }

    static final String getBatchesForPropIds() {
        """        
        SELECT CNST1.PMO_PROPOSAL_ID, 
        CNST1.NAME,
        CNST1.STATUS,
        CNST1.DESIGN_ID AS BATCH_ID,
        CNST1.CNT_COUNT,
        CNST1.SUM_BPS,
        CNST2.CNT_COUNT_DELIVERABLE,
        CNST2.SUM_BPS_DELIVERABLE
        FROM
                ( ${getSql()} ) cnst1 
        LEFT JOIN
                ( ${getSql("_DELIVERABLE","AND cs.cnst_status = 'completed'")} ) cnst2
        ON
      cnst1.name = cnst2.name AND
      cnst1.status = cnst2.status AND
      cnst1.design_id = cnst2.design_id
"""
    }
    static final String getConstructsPropIds() {
        """
        SELECT DISTINCT DC.DESIGN_ID,
  FD_STATUS,
  TO_CHAR(FD_CREATED,'YYYY/MM/DD') AS FD_CREATED,
  JGI_NAME,
  UNAME,
  TO_CHAR(COMPLETED_DATE, 'YYYY/MM/DD') AS COMPLETED_DATE,
  case when ORDERED_DATE NOTNULL then TO_CHAR(BATCH_LATEST_ORDERED_DATE, 'YYYY/MM/DD') else TO_CHAR(ORDERED_DATE, 'YYYY/MM/DD') end as ORDERED_DATE,
  case when RECEIVED_DATE NOTNULL then TO_CHAR(BATCH_LATEST_RECEIVED_DATE, 'YYYY/MM/DD')  else TO_CHAR(RECEIVED_DATE, 'YYYY/MM/DD') end as RECEIVED_DATE,
  CNST_P_SIZE
FROM DIM_CONSTRUCTS DC
  JOIN DIM_BATCHES DB ON DC.DESIGN_ID = DB.BATCH_ID
WHERE DB.PMO_PROPOSAL_ID in (:pmoPropIds)
"""
    }
}
