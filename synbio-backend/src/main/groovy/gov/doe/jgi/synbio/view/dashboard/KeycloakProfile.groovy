package gov.doe.jgi.synbio.view.dashboard

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import groovy.transform.Canonical
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@Canonical
class KeycloakProfile {
    String username
    String firstName
    String lastName
    String email
    Boolean enabled
}
