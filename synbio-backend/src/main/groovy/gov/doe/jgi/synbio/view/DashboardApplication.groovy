package gov.doe.jgi.synbio.view

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient

@SpringBootApplication
class DashboardApplication {

	static void main(String[] args) {
		SpringApplication.run(DashboardApplication, args)
	}

	@Value("\${signonUrl}")
	private String signonUrl

	@Bean
	WebClient wsAccessClient() {
		ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
				.codecs({ configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 1000 * 100) }).build();
		WebClient.builder().exchangeStrategies(exchangeStrategies).baseUrl(signonUrl)
		//.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.filter(logRequest())
				.build()
	}

	final static Logger LOGGER = LoggerFactory.getLogger(DashboardApplication.class)

	private ExchangeFilterFunction logRequest() {
		return { clientRequest, next ->
			LOGGER.info("Request: ${clientRequest.method()}" + " ${clientRequest.url()}")
			clientRequest.headers()
					.forEach(
							{ name, values -> values.forEach({ value -> LOGGER.info("$name $value") }) })
			return next.exchange(clientRequest);
		};
	}



}
