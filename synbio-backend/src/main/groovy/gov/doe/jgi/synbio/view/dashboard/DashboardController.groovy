package gov.doe.jgi.synbio.view.dashboard

import gov.doe.jgi.synbio.view.utils.CalibanForbiddenException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping(DASHBOARD_V1_API)
class DashboardController {
    static final Logger log = LoggerFactory.getLogger(DashboardController.class)
    public static final String DASHBOARD_V1_API = "synbio-view/api/v1/"

    @Autowired
    DashboardDataService dashboardDataService

/* uncomment for KC
    @GetMapping("batches")
    Collection<BatchDim> getBatches(Principal principal) {
        KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) principal
        KeycloakSecurityContext keycloakSecurityContext = token.account.keycloakSecurityContext
        AccessToken accessToken = keycloakSecurityContext.token
        String email = accessToken.email
        log.info("getConstructs>> user: ${token?.name}, ${accessToken.name}, ${accessToken.givenName}, ${accessToken.familyName}, " +
                "${email}, subject: ${accessToken.subject}")
        dashboardDataService.getBatchesForContactEmail(email)
    }
 */

    @GetMapping("batches")
    Collection<BatchDim> getBatches(@CookieValue(name = "jgi_session") String ssoSessionToken) {
        assert ssoSessionToken
        KeycloakProfile keycloakProfile = dashboardDataService.getKeycloakProfile(ssoSessionToken)
        log.info("getBatches>> $keycloakProfile")
        dashboardDataService.getBatchesForContactEmail(keycloakProfile.email)
    }

    @GetMapping("batches-for-contact-email")
    Collection<BatchDim> getBatchesForContactEmail(@CookieValue(name = "jgi_session") String ssoSessionToken,
                                                   @RequestParam(value="email") String email) {
        assert ssoSessionToken
        KeycloakProfile keycloakProfile = dashboardDataService.getKeycloakProfile(ssoSessionToken)
        if (!keycloakProfile.enabled)
            throw new CalibanForbiddenException(ssoSessionToken)
        dashboardDataService.getBatchesForContactEmail(email)
    }

/* uncomment for KC
    @Value("\${keycloak.auth-server-url}realms/jgi-web/account")
    private String accountUrl

    @GetMapping("account")
    void getAccount(HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location", accountUrl);
        httpServletResponse.setStatus(302);
    }
 */

    @GetMapping("account")
    KeycloakProfile getAccount(@CookieValue(name = "jgi_session") String ssoSessionToken) {
        assert ssoSessionToken
        KeycloakProfile keycloakProfile = dashboardDataService.getKeycloakProfile(ssoSessionToken)
        keycloakProfile
    }
}
