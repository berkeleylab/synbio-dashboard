package gov.doe.jgi.synbio.view.dashboard

interface DashboardDataService {
    Collection<BatchDim> getBatches()
    Collection<BatchDim> getBatchesForContactEmail(String email)
    Collection<Proposals> getProposalsForContactEmail(String email)
    KeycloakProfile getKeycloakProfile(String ssoSessionToken)
    Collection<SynbioUser> getActiveStaffSynbioUsersForContactEmail(String email)
}