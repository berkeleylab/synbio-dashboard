package gov.doe.jgi.synbio.view.dashboard

import groovy.transform.Canonical

@Canonical
class DashboardFeeder {
    Collection<BatchDim> batches
    Map<Integer, ConstructDim> constructs
}
