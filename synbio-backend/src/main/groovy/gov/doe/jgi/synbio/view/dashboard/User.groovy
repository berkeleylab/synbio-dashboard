package gov.doe.jgi.synbio.view.dashboard

import groovy.transform.Canonical

@Canonical
class User {
    Long id
    String name
    String password
    String email
    Boolean active
    Date createdDate
}
