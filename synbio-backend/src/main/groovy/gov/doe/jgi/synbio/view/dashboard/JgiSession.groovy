package gov.doe.jgi.synbio.view.dashboard

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
@JsonIgnoreProperties(ignoreUnknown = true)
class JgiSession {
    String ip
    String id
    @JsonProperty("user")
    SsoUser ssoUser
}
