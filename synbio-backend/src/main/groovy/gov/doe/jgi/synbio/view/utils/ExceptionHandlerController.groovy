package gov.doe.jgi.synbio.view.utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MissingRequestCookieException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandlerController {

    static final Logger log = LoggerFactory.getLogger(ExceptionHandlerController.class)

    @ExceptionHandler
    ResponseEntity<String> handle(CalibanInvalidSessionException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.UNAUTHORIZED)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(CalibanForbiddenException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.FORBIDDEN)
    }

    @ExceptionHandler
    ResponseEntity<String> handle(MissingRequestCookieException ex) {
        log.error(ex.message, ex)
        new ResponseEntity<String>(ex.message, HttpStatus.UNAUTHORIZED)
    }



}
