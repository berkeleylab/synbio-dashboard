package gov.doe.jgi.synbio.view.utils
/**
 * Created by iratnere on 11/7/14.
 */
class CalibanForbiddenException extends RuntimeException {
    CalibanForbiddenException(String message) {
        super(message)
    }
}
