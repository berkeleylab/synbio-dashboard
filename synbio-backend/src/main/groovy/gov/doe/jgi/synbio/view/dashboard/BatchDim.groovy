package gov.doe.jgi.synbio.view.dashboard

import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.Canonical

@Canonical
class BatchDim {
    Long pmoProposalId
    String name
    String status
    Integer batchId
    Integer counts
    Integer deliverableCounts
    Long basePairs
    Long deliverableBasePairs
    BigDecimal pCompleted
    BigDecimal pInProgress
    BigDecimal pWaiting
    BigDecimal pAbandoned
    @JsonProperty("constructDims")
    Collection<ConstructDim> constructs


    private static BigDecimal format(BigDecimal value) {
        (Integer) (value * 10)/10
    }
    void update(Collection<ConstructDim> constructs) {
        this.constructs = constructs
        Integer total = constructs.size()
        pCompleted = format( constructs.findAll {
            it.fdStatus == 'completed'
        }.size() / total * 100)

        pInProgress = format( constructs.findAll {
            it.fdStatus == 'inprogress' && it.receivedDate
        }.size() / total * 100 )

        pWaiting = format( constructs.findAll {
            it.fdStatus == 'inprogress' && it.receivedDate == null
        }.size() / total * 100 )

        pAbandoned = format ( constructs.findAll {
            it.fdStatus == 'abandoned'
        }.size() / total * 100 )
    }

    @Override
    public String toString() {
        return "BatchDim{" +
                "pmoProposalId=" + pmoProposalId +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", batchId=" + batchId +
                ", counts=" + counts +
                ", deliverableCounts=" + deliverableCounts +
                ", basePairs=" + basePairs +
                ", deliverableBasePairs=" + deliverableBasePairs +
                ", pCompleted=" + pCompleted +
                ", pInProgress=" + pInProgress +
                ", pWaiting=" + pWaiting +
                ", pAbandoned=" + pAbandoned +
                ", constructDims=" + constructs +
                '}';
    }
}
