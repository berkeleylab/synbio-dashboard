package gov.doe.jgi.synbio.view.dashboard

import groovy.transform.Canonical

@Canonical
class Dashboard {
    DashboardFeeder data
}
