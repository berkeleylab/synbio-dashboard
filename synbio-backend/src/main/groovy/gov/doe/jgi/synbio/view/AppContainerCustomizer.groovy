package gov.doe.jgi.synbio.view

import org.springframework.boot.web.server.ConfigurableWebServerFactory
import org.springframework.boot.web.server.WebServerFactoryCustomizer
import org.springframework.stereotype.Component

@Component
class AppContainerCustomizer implements WebServerFactoryCustomizer< ConfigurableWebServerFactory > {

    @Override
    void customize(ConfigurableWebServerFactory factory) {
        factory.setPort(8000);
    }
}
