package gov.doe.jgi.synbio.view.utils
/**
 * Created by iratnere on 11/7/14.
 */
class CalibanInvalidSessionException extends RuntimeException {
    CalibanInvalidSessionException(String message) {
        super(message)
    }
}
