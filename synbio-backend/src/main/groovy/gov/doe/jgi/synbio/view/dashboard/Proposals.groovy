package gov.doe.jgi.synbio.view.dashboard


import groovy.transform.Canonical

@Canonical
class Proposals {
    Long proposalId
    String focusArea
    String projectType
    String title

    @Override
    public String toString() {
        return "Proposals{" +
                "proposalId=" + proposalId +
                ", focusArea='" + focusArea + '\'' +
                ", projectType='" + projectType + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
