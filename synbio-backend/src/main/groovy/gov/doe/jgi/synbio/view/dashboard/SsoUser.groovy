package gov.doe.jgi.synbio.view.dashboard

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import groovy.transform.ToString

@ToString(includeNames = true, includeFields = true)
@JsonIgnoreProperties(ignoreUnknown = true)
class SsoUser {
    String login
    String first_name
    String last_name
    String email_address
//    Boolean internal

    KeycloakProfile toKeycloakProfile() {
        new KeycloakProfile(
                username: login,
                firstName: first_name,
                lastName: last_name,
                email: email_address
        )
    }
}
