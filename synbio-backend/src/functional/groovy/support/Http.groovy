package support

import wslite.rest.ContentType
import wslite.rest.RESTClient

class Http {
    static primeRestClient() {
        def port = 8000//new File('application.port').text
        def hostName = System.env.SPRING_REMOTE_HOST ?: 'localhost'
        def url = "http://$hostName"
        if (port)
            url += ":$port"
        println "url: $url"
        new RESTClient(url)
    }
    static getProjectResources(RESTClient client, String base, String spId, String resource) {
        client.get(path: "/$base/${spId}/${resource}")
    }


    static getResourceById(RESTClient client, String base, int id) {
        client.get(path: "/$base/${id}")
    }

    static putContent(RESTClient client, String base, int id, def content) {
        def response = client.put(path: "/$base/${id}") {
            type ContentType.JSON
            json content
        }
        response
    }


    static putContent(RESTClient client, String base, def content) {
        def response = client.put(path: "/$base") {
            type ContentType.JSON
            json content
        }
        response
    }

    static postContent(RESTClient client, String base, def content) {
        def response = client.post(path: "/$base") {
            type ContentType.JSON
            json content
        }
        response
    }


    static getResource(RESTClient client, String pathParam, def id = null) {
        def path = "$pathParam"
        if (id)
            path += "/${id}"
        client.get(path: path)
    }

    static getPruMetadata(RESTClient client, String key, String value) {
        client.get(path: "/physical-runs/physical-run-units?$key=$value")
    }

}
