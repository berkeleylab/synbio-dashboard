package gov.doe.jgi.synbio.view.dashboard

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.transaction.annotation.Transactional

//@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class DashboardDataServiceImplTest {
    static final Logger log = LoggerFactory.getLogger(DashboardDataServiceImplTest.class)

    @Autowired
    DashboardDataService dashboardDataService

    ObjectMapper objectMapper

    @BeforeEach
    void setup() {
        Jackson2ObjectMapperBuilder jacksonBuilder = new Jackson2ObjectMapperBuilder()
        objectMapper = jacksonBuilder.build()
    }

    @Test
    void "serialize signon response"() {
        given:
        when:
        JgiSession jgiSession = objectMapper.readValue(content, JgiSession.class)
        KeycloakProfile keycloakProfile = jgiSession.ssoUser.toKeycloakProfile()
        then:
        assert keycloakProfile
        log.info("$keycloakProfile")
    }

    final private String content =
            """
{
    "ip": "131.243.151.171",
    "id": "5d1c53ede3cc5f4ce5fb46fef74c7f3a",
    "user": {
        "id": 157,
        "created_at": null,
        "updated_at": "2015-05-28T21:41:01Z",
        "login": "iratnere",
        "email": "IRatnere@lbl.gov",
        "last_authenticated_at": "2022-02-14T18:19:30Z",
        "contact_update_required": null,
        "failed_login_at": null,
        "num_failed_logins": 0,
        "contact_id": 149,
        "replaced_by": null,
        "replaces": [],
        "prefix": null,
        "first_name": "Igor",
        "middle_name": null,
        "last_name": "Ratnere",
        "suffix": null,
        "gender": null,
        "institution": "Joint Genome Institute",
        "institution_type": "Academic",
        "department": null,
        "address_1": "2800 Mitchell Drive",
        "address_2": "",
        "city": "Walnut Creek",
        "state": "CA",
        "postal_code": "94598",
        "country": "United States",
        "phone_number": "9259272520",
        "fax_number": null,
        "email_address": "IRatnere@lbl.gov",
        "comments": null,
        "funding_sources": null,
        "institution_title": null,
        "institution_title_comment": null,
        "small_business": null,
        "kbase_username": null,
        "orcid_id": null,
        "internal": true
    }
}
"""



    @Test
    void "retrieve batches for contact email"() {
        given:
//        String email = 'yung6@llnl.gov'
        String email = 'srhee@carnegiescience.edu' //in progress
//        String email = 'HOtani@lbl.gov' //completed
        when:
        Collection<BatchDim> batches = dashboardDataService.getBatchesForContactEmail(email)
        then:
        assert batches
        log.info("$batches")
    }

    @Test
    void "retrieve proposals for contact email"() {
        given:
        String email = 'HOtani@lbl.gov'
        when:
        Collection<Proposals> proposals = dashboardDataService.getProposalsForContactEmail(email)
        then:
        assert proposals
        log.info("$proposals")
    }

    @Test
    void "retrieve active staff synbio users for contact email"() {
        given:
        String email = 'xianweimeng@lbl.gov'
        when:
        SynbioUser synbioUser = dashboardDataService.getActiveStaffSynbioUsersForContactEmail(email).find {true }
        then:
        assert synbioUser
        log.info("$synbioUser")
    }

}